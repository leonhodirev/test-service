<?php

/* @var $this yii\web\View */

$this->title = 'Техническое задание';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Добро пожаловать!</h1>

        <p class="lead">Техническое задание</p>

        <p><a class="btn btn-lg btn-success" href="/service/index">Перейти в услуги</a></p>
    </div>

    </div>
</div>

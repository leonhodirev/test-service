<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $id integer */

$this->title = 'История сервиса';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$dataProvider = new ActiveDataProvider([
    'query' => \common\models\ServiceHistory::find()->where(['service_id' => $id])->orderBy('id DESC'),
]);
?>
<div class="service-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--    --><? //= DetailView::widget([
    //        'model' => $model,
    //        'attributes' => [
    //            'id',
    //            'name',
    //            'code',
    //            'price',
    //            'description',
    //            'status',
    //            'term',
    //            'city',
    //        ],
    //    ]) ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'column',
                'content' => function ($data) {
                    return $data->service->attributeLabels()[$data->column];
                },
            ],
            'previous_value',
            'value',
            'date',
            [
                'attribute' => 'user_id',
                'label' => 'Пользователь',
                'content' => function ($data) {
                    return $data->user->username;
                },
            ],
//            'user_id' => function ($data) {
//                return json_decode($data->user->username);
//            },
            //'status',
            //'term',
            //'city',
        ],
    ]); ?>

</div>

<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $createService = $auth->createPermission('createService');
        $createService->description = 'Create a service';
        $auth->add($createService);

        $updateServiceStatus = $auth->createPermission('updateServiceStatus');
        $updateServiceStatus->description = 'Update service status';
        $auth->add($updateServiceStatus);

        $administrator = $auth->createRole('administrator');
        $auth->add($administrator);
        $auth->addChild($administrator, $createService);
        $auth->addChild($administrator, $updateServiceStatus);

        $operator = $auth->createRole('operator');
        $auth->add($operator);

        $auth->assign($administrator, 1);
        $auth->assign($operator, 2);
    }
}
<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'verification_token' => $this->string()->defaultValue(null),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $tableOptions);

        $this->insert('user', [
            'username' => 'test-1',
            'auth_key' => 'xwFAei7RZlKiVvgTGqI_gtJp06HuFcJn',
            'password_hash' => '$2y$13$VYfzcNNwtwqStmjUxp8B0ujgzVEkDBcyfOKnTa96wodKw2nP0/Fg.',
            'email' => 'test@test.ru',
            'status' => 10,
            'created_at' => '2019-05-22 09:00:00',
            'updated_at' => '2019-05-22 09:00:00'
        ]);

        $this->insert('user', [
            'username' => 'test-2',
            'auth_key' => 'xwFAei7RZlKiVvgTGqI_gtJp06HuFcJn',
            'password_hash' => '$2y$13$VYfzcNNwtwqStmjUxp8B0ujgzVEkDBcyfOKnTa96wodKw2nP0/Fg.',
            'email' => 'test2@test.ru',
            'status' => 10,
            'created_at' => '2019-05-22 09:00:00',
            'updated_at' => '2019-05-22 09:00:00'
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}

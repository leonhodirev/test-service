<?php

use yii\db\Migration;

class m190522_052709_service extends Migration
{
    public function up()
    {
        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string(),
            'price' => $this->string(),
            'description' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'term' => $this->dateTime(),
            'city' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%service}}');
    }
}

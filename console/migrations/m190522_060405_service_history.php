<?php

use yii\db\Migration;

class m190522_060405_service_history extends Migration
{
    public function up()
    {
        $this->createTable('{{%service_history}}', [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer()->notNull(),
            'column' => $this->string()->notNull(),
            'previous_value' => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
            'date' => $this->dateTime()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex('idx_service_history_user_id', 'service_history', 'user_id');

        // add foreign key for table `user`
        $this->addForeignKey('fx_service_history_user_id', 'service_history', 'user_id',
            'user', 'id');

        // creates index for column `service_id`
        $this->createIndex('idx_service_history_service_id', 'service_history', 'service_id');

        // add foreign key for table `service`
        $this->addForeignKey('fx_service_history_service_id', 'service_history', 'service_id',
            'service', 'id');
    }

    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey('fx_service_history_user_id', 'service_history');

        // drops index for column `user_id`
        $this->dropIndex('idx_service_history_user_id', 'service_history');

        // drops foreign key for table `service`
        $this->dropForeignKey('fx_service_history_service_id', 'service_history');

        // drops index for column `service_id`
        $this->dropIndex('idx_service_history_service_id', 'service_history');

        $this->dropTable('{{%service_history}}');
    }
}

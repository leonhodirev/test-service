<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $price
 * @property string $description
 * @property int $status
 * @property string $term
 * @property string $city
 *
 * @property ServiceHistory[] $serviceHistories
 */
class Service extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const STATUSES = [
        self::STATUS_ACTIVE => 'Включена',
        self::STATUS_INACTIVE => 'Выключена'
    ];

    public static function tableName()
    {
        return 'service';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['term'], 'safe'],
            [['name', 'code', 'price', 'description', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'price' => Yii::t('app', 'Price'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'term' => Yii::t('app', 'Term'),
            'city' => Yii::t('app', 'City'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceHistories()
    {
        return $this->hasMany(ServiceHistory::className(), ['service_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (!$this->isNewRecord) {
            foreach ($this->attributes as $key => $attribute) {
                foreach ($this->oldAttributes as $oldKey => $oldAttribute) {
                    if ($key === $oldKey && trim($attribute) != trim($oldAttribute)) {
                        $log = new ServiceHistory();
                        $log->service_id = $this->id;
                        $log->column = $key;
                        $log->previous_value = $oldAttribute;
                        $log->value = $attribute;
                        $log->user_id = Yii::$app->user->identity->id;
                        $log->date = date('Y-m-d H:i:s');
                        $log->save();
                    }
                }
            }
        }
        return parent::beforeSave($insert);
    }
}

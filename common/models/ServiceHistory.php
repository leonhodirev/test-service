<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service_history".
 *
 * @property int $id
 * @property int $service_id
 * @property string $column
 * @property string $previous_value
 * @property string $value
 * @property string $date
 * @property int $user_id
 *
 * @property Service $service
 * @property User $user
 */
class ServiceHistory extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'service_history';
    }

    public function rules()
    {
        return [
            [['service_id', 'column', 'previous_value', 'value', 'date', 'user_id'], 'required'],
            [['service_id', 'user_id'], 'integer'],
            [['date'], 'safe'],
            [['column', 'previous_value', 'value'], 'string', 'max' => 255],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'service_id' => Yii::t('app', 'Service ID'),
            'column' => Yii::t('app', 'Column'),
            'previous_value' => Yii::t('app', 'Previous Value'),
            'value' => Yii::t('app', 'Value'),
            'date' => Yii::t('app', 'Date'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

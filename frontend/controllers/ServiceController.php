<?php

namespace frontend\controllers;

use common\models\Service;
use \yii\rest\Controller;
use yii\data\ActiveDataProvider;

class ServiceController extends Controller
{
    public $modelClass = 'common\models\Service';

    public function actionIndex($city)
    {
        return new ActiveDataProvider([
            'query' => Service::find()->where(['status' => 1, 'city' => $city]),
        ]);
    }

    public function actionView($id)
    {
        return Service::find()->where(['status' => 1, 'id' => $id])->one();
    }
}